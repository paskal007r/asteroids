using System.Collections;
using UnityEngine;

/*
 * This class manages the player's spaceship. It requests the dependency injection for the input system to the InputManagerDIC.
 * It has also the responsibility of informing the GameStateManager of player deaths
 */
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(WeaponManager))]
[RequireComponent(typeof(ParticleSystem))]
public class SpaceShipManager : MonoBehaviour
{
    #region managed components
    [SerializeField]
    [HideInInspector]
    Rigidbody2D rigidBodyComponent;
    [SerializeField]
    [HideInInspector]
    Collider2D colliderComponent;
    [SerializeField]
    [HideInInspector]
    SpriteRenderer spriteRendererComponent;
    [SerializeField]
    ParticleSystem rocketParticleComponent;
    [SerializeField]
    ParticleSystem deathParticleComponent;
    #endregion

    bool isFiring;

    [SerializeField]
    [Range(0.01f, 10)]
    float ThrustForceFactor = 1;
    bool isThrusting;

    [SerializeField]
    float HyperspaceCooldown = 1;
    float lastHyperspaceTime = -1000f;

    [SerializeField]
    WeaponManager weapon;

    public void Reset()
    {
        rigidBodyComponent = GetComponent<Rigidbody2D>();
        colliderComponent = GetComponent<Collider2D>();
        weapon = GetComponent<WeaponManager>();
        spriteRendererComponent = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    void Awake()
    {
        //As the level is loaded request to the Input Manager's Dependency Injectior Class to register the reactions for each input event
        InputManagerDIC.RegisterInputRequest(
            updateDirection,
            activateFire,
            deactivateFire,
            activateThrust,
            deactivateThrust,
            activateHyperspace);
    }

    /// <summary>
    /// read the direction of the controller and turn in that exact direction
    /// </summary>
    /// <param name="direction"></param>
    void updateDirection(Vector2 direction)
    {
        transform.rotation = Quaternion.Euler(0, 0, Vector2.Angle(Vector2.down, direction) * (direction.x > 0 ? 1 : -1) - 180);
    }

    /// <summary>
    /// start firing with the weapon by activating the appropriate flag in the update cycle
    /// </summary>
    void activateFire() { isFiring = true; }
    /// <summary>
    /// stop weapon fire
    /// </summary>
    void deactivateFire() { isFiring = false; }

    /// <summary>
    /// start pushing the spaceship by activating the appropriate flag in the update cycle and the rocket's particle effect
    /// </summary>
    void activateThrust()
    {
        rocketParticleComponent.Play();
        isThrusting = true;
    }

    /// <summary>
    /// stop pushing the spaceship and the rocket's particle effect
    /// </summary>
    void deactivateThrust()
    {
        rocketParticleComponent.Stop();
        isThrusting = false;
    }

    /// <summary>
    /// Instantaneously teleport the spaceship in a random position on the screen, at own risk
    /// </summary>
    void activateHyperspace()
    {
        if (lastHyperspaceTime + HyperspaceCooldown <= Time.time)
        {
            rigidBodyComponent.velocity = Vector2.zero;
            transform.position = CoordinateUtility.getRandomScreenPosition();
            lastHyperspaceTime = Time.time;
        }
    }

    public void Update()
    {
        //executes the physics of the thrust
        if (isThrusting)
        {
            rigidBodyComponent.AddRelativeForce(Vector2.up * ThrustForceFactor);
        }
        //executes the firing
        if (isFiring)
        {
            weapon.Fire();
        }
    }

    /// <summary>
    /// checks for collisions that may kill the player. Except for the border every collision can destroy the spaceship
    /// Collision with friendly particles is avoided both by the physics2d settings and the lifetime of the bullets
    /// </summary>
    /// <param name="collision"></param>
    public void OnTriggerEnter2D(Collider2D collision)
    {
        //everything but the border can kill the player.
        if (!collision.CompareTag(TagNames.Border.ToString()))
        {
            StartCoroutine(processDeath());
        }
    }
    /// <summary>
    /// in case of death a particle is shown for a few seconds then the position is reset at the center of the screen with 0 speed
    /// in this time, the spaceship cannot be hit
    /// This method is also responsible for informing the GameStateManager of the death event
    /// </summary>
    /// <returns></returns>
    IEnumerator processDeath()
    {
        colliderComponent.enabled = false;
        spriteRendererComponent.enabled = false;
        deathParticleComponent.Play();
        isThrusting = false;
        isFiring = false;
        rocketParticleComponent.Stop();
        SoundManager.instance.requestSound(SoundManager.sounds.playerDeath);
        yield return new WaitForSeconds(1.5f);
        //Debug.Log("dead");
        GameStateManager.instance.OnPlayerDeath();
        deathParticleComponent.Stop();
        colliderComponent.enabled = true;
        spriteRendererComponent.enabled = true;
        rigidBodyComponent.velocity = Vector2.zero;
        transform.position = Vector3.zero;
    }
}
