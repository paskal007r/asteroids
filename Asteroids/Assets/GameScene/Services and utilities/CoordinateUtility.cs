using UnityEngine;

/*
 * Utility class with functions used to deal with the mathematical transformations needed to deal with 2d coordinate space transformations
 */
public static class CoordinateUtility
{
    const float VERTICALVIEWPORTPADDING = 0.05f;//padding on x axis
    const float HORIZONTALVIEWPORTPADDING = 0.1f;//padding on y axis
    public static Vector3 getRandomBorderPosition(float Extension)
    {
        float normalizedSize = getNormalizedSize(Extension);
        //initialize result vector (will be interpreted as Viewport, z=1 so that it's inside camera culling volume)
        Vector3 result = Vector3.forward;
        //coin toss to decide wether to pick a top/bottom location or a left/right one
        if (Random.Range(0f, 1f) > 0.5f)
        {
            //top/bottom
            result += Vector3.right * Random.Range(0f, 1f);
            //coin toss to decide wether it goes on top or bottom.
            result += new Vector3(0, getPaddedViewportPos(Random.Range(0f, 1f) > 0.5f ? 1 : 0, normalizedSize), 0);
        }
        else
        {
            //left/right
            result += Vector3.up * Random.Range(0f, 1f);
            //coin toss to decide wether it goes on left or right.
            result += new Vector3(getPaddedViewportPos(Random.Range(0f, 1f) > 0.5f ? 1 : 0, normalizedSize), 0, 0);
        }

        //Debug.Log("Generate coord viewport: " + result.ToString());

        return Camera.main.ViewportToWorldPoint(result);
    }

    static float getNormalizedSize(float ToNormalize)
    {
        return 0.25f * Camera.main.WorldToViewportPoint(Camera.main.ViewportToWorldPoint(Vector2.zero) + Vector3.right * ToNormalize).x;
    }

    public static Vector3 getOppositeBorderPosition(Vector3 position, bool isVertical, float Extension)
    {
        float normalizedSize = getNormalizedSize(Extension);
        Vector3 result = Camera.main.WorldToViewportPoint(position);//calculate opposite side of screen position
        if (isVertical)
            result = new Vector3(getPaddedViewportPos(Mathf.RoundToInt(1 - result.x), normalizedSize), result.y, result.z);
        else
            result = new Vector3(result.x, getPaddedViewportPos(Mathf.RoundToInt(1 - result.y), normalizedSize), result.z);

        return Camera.main.ViewportToWorldPoint(result);
    }

    static float getPaddedViewportPos(float baseValue, float Extension)
    {
        if (baseValue > 0.01f && baseValue < 0.99f)
            Debug.LogError("WARING getPaddedViewportPos is supposed to be only used with values of 1 or 0");

        return baseValue + Extension * ((baseValue > 0.5f) ? -1 : 1);
    }


    public static Vector3 getRandomScreenPosition()
    {
        // z=1 so that it's inside camera culling volume
        return Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(HORIZONTALVIEWPORTPADDING, 1 - HORIZONTALVIEWPORTPADDING), Random.Range(VERTICALVIEWPORTPADDING, 1 - VERTICALVIEWPORTPADDING), 1));
    }

}
