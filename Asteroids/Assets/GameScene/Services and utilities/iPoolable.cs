
/*
 * This interface must be implemented by (exactly) one script at the root of any pooled gameobject
 */
public interface iPoolable
{
    //reference to the pool to wich the pooled object belongs (and to wich it should return when disposed)
    Pool source { get; set; }
    //initialization method called by the Allocate function
    void Initialize();
    //reset method called by the PutInPool function
    void Deactivate();
}
