using UnityEngine;

public class PoolHost : MonoBehaviour
{
    public static PoolHost instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    public static void Hold(GameObject poolable)
    {
        poolable.transform.parent = instance.transform;
    }
}
