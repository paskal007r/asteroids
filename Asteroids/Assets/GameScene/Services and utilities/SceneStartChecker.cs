using UnityEngine;

public class SceneStartChecker : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (GameStateManager.instance == null)
            LevelNames.MenuScene.Load();
    }

}
