using System.Collections.Generic;
using UnityEngine;
/*
 * This data class manages the Pooling of one type of prefab.
 * When the "populate" method is called it will initialize its data structure and
 * instantiate an a number of "initialPoolSize" instances of the "prefabReference" prefab. 
 * These parameters are supposed to be set with the inspector.
 * Pooled objects must have on their root a script that implements the iPoolable interface and 
 * is responsible for initializing and resetting their state.
 * The objects in the pool are kept with SetActive(false) and parented to a dedicated 
 * gameobject to which the PoolHost singleton is attached
 */
[System.Serializable]
public class Pool
{
    Stack<GameObject> pool;
    [SerializeField]
    int initialPoolSize;
    [SerializeField]
    int expansionPoolSize;
    [SerializeField]
    GameObject prefabReference;

    /// <summary>
    /// initialize the pool's population and data
    /// </summary>
    public void Populate()
    {
        pool = new Stack<GameObject>();
        ExpandPoolPopulation(initialPoolSize, prefabReference);
    }

    /// <summary>
    /// actually instantiates prefab instances and adds them to the pool stack
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="prefabReference"></param>
    void ExpandPoolPopulation(int amount, GameObject prefabReference)
    {
        for (int j = 0; j < amount; j++)
        {
            GameObject result = GameObject.Instantiate(prefabReference);
            result.GetComponent<iPoolable>().source = this;
            PutInPool(result);
        }
    }

    /// <summary>
    /// this method is the appropriate way to request an instance of the pooled gameobjects
    /// Will check if the pool if empity and eventually expand it before serving an initialized instance as result
    /// </summary>
    /// <returns></returns>
    public GameObject Allocate()
    {
        //if pool is empity, expand it
        if (pool.Count <= 0)
            ExpandPoolPopulation(expansionPoolSize, prefabReference);

        //get instance from pool
        GameObject result = pool.Pop();
        //initialize instance
        result.SetActive(true);
        result.GetComponent<iPoolable>().Initialize();
        return result;
    }

    /// <summary>
    /// this method is the appropriate way to dispose of an instance of a pooled object
    /// It will reset all of its transform data, set it inactive and call the Deactivate method on them
    /// </summary>
    /// <param name="handled"></param>
    public void PutInPool(GameObject handled)
    {
        //reset state
        handled.GetComponent<iPoolable>().Deactivate();

        //deactivate object
        handled.SetActive(false);
        //reset transform values
        handled.transform.position = Vector3.zero;
        handled.transform.rotation = Quaternion.identity;
        handled.transform.localScale = Vector3.one;
        //reparent the object to the Pool host
        PoolHost.Hold(handled);
        //add it again to the object pool
        pool.Push(handled);
    }
}