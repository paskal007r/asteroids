using UnityEngine;

/*
 * This class manages the firing behaviour of a weapon. It can be controlled through a number of variables
 * with wich radically different firing behaviours can be obtained. 
 * For instance:
 * -high range, precise, long cooldown weapon (FireInterval=2f,accuracy=1f,bulletSpeed=10f)
 * -uzi-like (FireInterval=.05f,accuracy=.75f,bulletSpeed=5f)
 * -damage aura-like (FireInterval=.025f,accuracy=0f,bulletSpeed=3f)
 * The projectilePool can also be used to set different types of bullets to be fired
 * 
 * To fire this weapon is sufficient to call the Fire method, it will deal on its own with the time interval between shots
 */
public class WeaponManager : MonoBehaviour
{

    [SerializeField]
    public float FireInterval = .25f;
    float lastShotTime;

    [SerializeField]
    [Range(0f, 1f)]
    public float accuracy = 1f;

    [SerializeField]
    public float bulletSpeed = 5f;

    [SerializeField]
    Pool projectilePool;

    public void Awake()
    {
        projectilePool.Populate(); lastShotTime = -1000f;
    }

    /// <summary>
    /// fires a bullet if the cooldown since last shot is over
    /// </summary>
    public void Fire()
    {
        if (lastShotTime + FireInterval <= Time.time)
        {
            //allocate bullet from pool
            GameObject bullet = projectilePool.Allocate();
            bullet.transform.position = transform.position;
            //changes the bullet's layer to the weapon's layer so that enemies won't hit each other and the player won't hit himself since 
            //the enemy-enemy collisions and the player-player collisions have been disabled in the Physics2D settings
            bullet.layer = transform.gameObject.layer;
            //add random rotation for imprecision
            bullet.transform.localRotation = Quaternion.Slerp(Quaternion.Euler(0, 0, Random.Range(0, 360)), transform.rotation, accuracy);
            //set initial velocity
            Rigidbody2D bulletBody = bullet.GetComponent<Rigidbody2D>();
            bulletBody.AddRelativeForce(Vector2.up * bulletSpeed, ForceMode2D.Impulse);

            //update last shot time
            lastShotTime = Time.time;
        }
    }
}
