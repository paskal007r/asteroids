using System.Collections;
using UnityEngine;

/// <summary>
/// This class manages a bullet's lifetime and implements the iPoolable interface for the bullet
/// </summary>
public class BulletManager : MonoBehaviour, iPoolable
{
    public AudioSource soundSource;
    public AudioClip[] Shot;

    public float lifeTime;
    public Pool source { get; set; }

    public void Deactivate()
    {
        StopAllCoroutines();
    }

    public void Initialize()
    {
        StartCoroutine(selfDestruct());
        SoundManager.instance.requestSound(SoundManager.sounds.shot);
    }

    IEnumerator selfDestruct()
    {
        yield return new WaitForSeconds(lifeTime);
        source.PutInPool(gameObject);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(TagNames.Hittable.ToString()))
            source.PutInPool(gameObject);
    }
}
