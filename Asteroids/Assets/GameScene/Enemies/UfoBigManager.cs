using System.Collections;
using UnityEngine;
/*
 * concrete class for big ufos, really slow, default weapon and they disappear when crossing the border.
 */
public class UfoBigManager : UfoManager
{

    [SerializeField]
    float speedFactor = 2f;

    public override void Initialize()
    {
        base.Initialize();
        StartCoroutine(StartMoving());
    }

    IEnumerator StartMoving()
    {
        yield return new WaitForEndOfFrame();
        rigidBodyComponent.velocity = (TargetReference.position - transform.position).normalized * speedFactor;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.CompareTag(TagNames.Border.ToString()))
        {
            source.PutInPool(gameObject);
        }
    }
}
