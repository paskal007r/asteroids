using System.Collections;
using UnityEngine;
/*
 * This class manages the behaviour of the asteroid type of enemy, and implements the iPoolable behaviour for it
 * An asteroid's speed and score is determined by its Mass wich goes from 1 (smallest) to 3 (biggest)
 * The bigger asteroids are slower and worth less points. Every time an asteroid is hit it's destroyed but if 
 * the mass is more than 1, two smaller child asteroids will be spawned.
 * This class has the responsibility of informing the GameStateManager of its destruction, of the score gained 
 * and eventually also of the spawning of the child asteroids
 */
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(Collider2D))]
public class AsteroidManager : MonoBehaviour, iPoolable
{
    #region variables
    [SerializeField]
    [Range(1, MAXMASS)]
    public int Mass;
    public Vector2 initialDirection;
    [SerializeField]
    float speedFactor = 5f;
    const int MAXMASS = 3;
    [SerializeField]
    [Tooltip("element 0 = mass 1, element 1 = mass 2, element 2 = mass 3")]
    int[] ScoreValueByMass = { 100, 50, 20 };
    [SerializeField]
    Sprite[] TextureOptionsSmall;
    [SerializeField]
    Sprite[] TextureOptionsMedium;
    [SerializeField]
    Sprite[] TextureOptionsBig;

    #region components
    [SerializeField]
    [HideInInspector]
    Rigidbody2D rigidbodyComponent;

    [SerializeField]
    [HideInInspector]
    Collider2D colliderComponent;

    [SerializeField]
    [HideInInspector]
    SpriteRenderer renderComponent;

    [SerializeField]
    [HideInInspector]
    ParticleSystem particleComponent;
    #endregion
    public void Reset()
    {
        rigidbodyComponent = GetComponent<Rigidbody2D>();
        renderComponent = GetComponent<SpriteRenderer>();
        colliderComponent = GetComponent<Collider2D>();
        particleComponent = GetComponent<ParticleSystem>();
        Mass = 1;
        initialDirection = Vector2.one;
    }

    public Pool source { get; set; }
    #endregion
    public void Initialize()
    {
        renderComponent.enabled = true;
        colliderComponent.enabled = true;
        Mass = MAXMASS;
        initialDirection = Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0f, 360f)) * Vector2.one;

        build();

    }

    public void build()
    {
        transform.localScale = Vector3.one * 5 * Mass / MAXMASS;
        SetInitialSpeed(initialDirection);
        setRandomSpriteByMass();

    }

    void SetInitialSpeed(Vector2 direction)
    {
        rigidbodyComponent.velocity = direction * speedFactor * (1 - Mass / (3 + MAXMASS));
    }

    void setRandomSpriteByMass()
    {
        switch ((int)Mass)
        {
            case 1:
                renderComponent.sprite = TextureOptionsSmall[Random.Range(0, TextureOptionsSmall.Length)];
                break;
            case 2:
                renderComponent.sprite = TextureOptionsMedium[Random.Range(0, TextureOptionsMedium.Length)];
                break;
            case 3:
                renderComponent.sprite = TextureOptionsBig[Random.Range(0, TextureOptionsBig.Length)];
                break;
            default:
                break;
        }
    }
    public void Deactivate()
    { }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(TagNames.Bullet.ToString()))
        {
            StartCoroutine(OnHit());
        }
    }

    IEnumerator OnHit()
    {
        //hide asteroid
        renderComponent.enabled = false;
        colliderComponent.enabled = false;
        //start particle emission
        particleComponent.Play();
        SoundManager.instance.requestSound(SoundManager.sounds.asteroidDeath);


        //if mass is not 1, then this asteroid must leave 2 children asteroids
        if (Mass > 1)
            for (int i = 0; i < 2; i++)
            {
                GenerateChildAsteroid(i);
            }
        yield return new WaitForSeconds(.5f);
        //stop particle emission
        particleComponent.Stop();
        yield return new WaitForSeconds(.5f);

        //inform game sate of asteroid elimination
        GameStateManager.instance.GainScore(ScoreValueByMass[(int)Mass - 1]);
        GameStateManager.instance.AsteroidDestroyed();

        //dispose of this asteroid
        source.PutInPool(gameObject);
    }

    void GenerateChildAsteroid(int index)
    {
        //allocate asteroid from source pool
        GameObject child = source.Allocate();
        //change its position to current position
        child.transform.position = transform.position;
        //initialize child's values
        AsteroidManager asteroidChildComponent = child.GetComponent<AsteroidManager>();
        asteroidChildComponent.Mass = Mass - 1;
        asteroidChildComponent.initialDirection = Quaternion.Euler(0f, 0f, 30 * Mathf.Pow(-1, index)) * initialDirection;
        //call build to override the values set from Initialize in the Pool's Allocate call
        asteroidChildComponent.build();
        GameStateManager.instance.AsteroidCreated();
    }
}
