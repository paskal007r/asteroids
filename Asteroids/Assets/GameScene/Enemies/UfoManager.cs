using System.Collections;
using UnityEngine;

/*
 * This is the abstract class that implements the shared behaviour and data of two kinds of enemies: small ufos and big ufos
 * it gives a base behaviour of a poolable enemy with a weapon that fires in bursts always aiming at the player and that can
 * be killed by a player's bullet
 */

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(ParticleSystem))]
public abstract class UfoManager : MonoBehaviour, iPoolable
{
    [SerializeField]
    [HideInInspector]
    protected Rigidbody2D rigidBodyComponent;
    [SerializeField]
    [HideInInspector]
    protected Collider2D colliderComponent;
    [SerializeField]
    [HideInInspector]
    protected SpriteRenderer spriteRendererComponent;
    [SerializeField]
    protected ParticleSystem deathParticleComponent;
    [SerializeField]
    [HideInInspector]
    protected WeaponManager weapon;

    [SerializeField]
    float fireBarrageLenght;
    [SerializeField]
    float fireBarrageInterval;
    [SerializeField]
    float lastFireBarrageStartTime;
    [SerializeField]
    protected Transform weaponHolder;

    protected Transform TargetReference;

    public int Score = 100;

    public Pool source { get; set; }

    public virtual void Reset()
    {
        rigidBodyComponent = GetComponent<Rigidbody2D>();
        colliderComponent = GetComponent<Collider2D>();
        spriteRendererComponent = GetComponent<SpriteRenderer>();
        deathParticleComponent = GetComponent<ParticleSystem>();
        weapon = weaponHolder.GetComponent<WeaponManager>();
    }

    public virtual void Deactivate()
    {
    }


    public virtual void Initialize()
    {
        //set initial state
        colliderComponent.enabled = true;
        spriteRendererComponent.enabled = true;
        rigidBodyComponent.velocity = Vector2.zero;
        lastFireBarrageStartTime = -1000;

        //if the player wasn't aquired in a previous allocation, aquire it as a target
        if (TargetReference == null)
            TargetReference = GameObject.FindGameObjectWithTag(TagNames.Player.ToString()).transform;
    }

    /// <summary>
    /// detect bullet collisions and process their hits
    /// </summary>
    /// <param name="collision"></param>
    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(TagNames.Bullet.ToString()))
        {
            StartCoroutine(OnHit());
        }
    }

    /// <summary>
    /// processes hit effects in two phases separated by a wait, so that particle effects and sounds can be played
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator OnHit()
    {
        StartOnHitEffects();
        yield return new WaitForSeconds(1.5f);
        //Debug.Log("dead");
        EndOnHitEffects();
    }
    protected virtual void StartOnHitEffects()
    {
        colliderComponent.enabled = false;
        spriteRendererComponent.enabled = false;
        deathParticleComponent.Play();
        SoundManager.instance.requestSound(SoundManager.sounds.ufoDeath);
    }
    protected virtual void EndOnHitEffects()
    {
        deathParticleComponent.Stop();
        GameStateManager.instance.GainScore(Score);
        source.PutInPool(gameObject);
    }

    protected virtual void Update()
    {

        if (lastFireBarrageStartTime + fireBarrageLenght > Time.time)
        {
            //point weapon towards player
            weaponHolder.LookAt(TargetReference, Vector3.back);
            weapon.Fire();
        }
        else
        {
            //wait for interval to end and reset timer
            if (lastFireBarrageStartTime + fireBarrageLenght + fireBarrageInterval < Time.time)
                lastFireBarrageStartTime = Time.time;
        }
    }
}
