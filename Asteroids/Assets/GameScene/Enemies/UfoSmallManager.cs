using UnityEngine;
/*
 * concrete class for small ufos, adds a random direction change behaviour and sets accuracy in function to the current level (caps at level)
 */
public class UfoSmallManager : UfoManager
{
    [SerializeField]
    float speedFactor = 5f;
    float lastDirectionChangeTime = -1000;
    [SerializeField]
    float directionChangeInterval = 2.5f;
    public override void Initialize()
    {
        base.Initialize();
        lastDirectionChangeTime = -1000;
        Score = 150;
        weapon.accuracy = Mathf.Min(1, GameStateManager.currentLevel / 10f);
    }
    protected override void Update()
    {
        base.Update();

        if (lastDirectionChangeTime + directionChangeInterval < Time.time)
        {
            rigidBodyComponent.velocity = Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0f, 360f)) * Vector2.one * speedFactor;
            lastDirectionChangeTime = Time.time;
        }
    }
}
