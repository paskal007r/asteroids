using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class has the responsibility of holding the enemy data, instantiating the enemies at the beginning of each level,
 * and to periodically spawn the enemies with a frequency of spawn (ufos)
 * For efficiency reasons all the enemies are instantiated through a pooling mechanism.
 * This class is a singleton.
 */
public class EnemyFactory : MonoBehaviour
{
    #region variables
    #region enemyData
    public List<EnemyData> enemyTypePrefabList = new List<EnemyData>();

    public Dictionary<enemyType, EnemyData> enemyDataDict;
    public void InitializeDict()
    {
        enemyDataDict = new Dictionary<enemyType, EnemyData>();

        foreach (var item in enemyTypePrefabList)
        {
            enemyDataDict.Add(item.type, item);
        }
    }
    #endregion

    public Dictionary<enemyType, int> EnemyAmount;

    public static EnemyFactory instance;
    #endregion
    public void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        //initialization functions
        InitializeDict();
        populatePools();
    }

    public void OnDestroy()
    {
        instance = null;
    }

    /// <summary>
    /// Pools need to be populated before anything can be instantiated, this function ensures that all pools are populated
    /// </summary>
    void populatePools()
    {
        for (int i = 0; i < enemyTypePrefabList.Count; i++)
        {
            EnemyData enemyToPopulate = enemyDataDict[enemyTypePrefabList[i].type];
            enemyToPopulate.pool.Populate();
        }

    }

    /// <summary>
    /// Function called at the beginning of a new level,
    /// the argument is a dictionary containing the number of enemies to spawn for each type
    /// </summary>
    /// <param name="enemies"></param>
    public void StartLevel(Dictionary<enemyType, int> enemies)
    {
        if (EnemyAmount != null)
            EnemyAmount.Clear();
        EnemyAmount = enemies;
        PopulateLevel();
    }

    /// <summary>
    /// This function parses the EnemyAmount data structure to generate the appropriate number of enemies 
    /// or start the right spawners for the enemies that mustn't appear at level start but should spawn
    /// throughout the level
    /// </summary>
    void PopulateLevel()
    {
        StopAllCoroutines();
        foreach (var item in EnemyAmount)
        {
            //get instance from pool
            if (enemyDataDict[item.Key].hasFrequency)
            {
                if (item.Value != 0f)
                    StartCoroutine(SpawnCoroutine(enemyDataDict[item.Key].totalSpawnTime / (float)item.Value, item.Key, item.Value));
            }
            else
            {
                for (int i = 0; i < item.Value; i++)
                {
                    generateEnemy(item.Key);
                }
            }
        }
    }

    /// <summary>
    /// Spawner coroutine, will spawn a number of "total" enemies of type "type", every "interval" seconds
    /// </summary>
    /// <param name="interval"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    IEnumerator SpawnCoroutine(float interval, enemyType type, int total)
    {
        int toSpawn = total;
        while (toSpawn > 0)
        {
            --toSpawn;
            yield return new WaitForSeconds(interval);
            generateEnemy(type);
        }
    }
    /// <summary>
    /// this method instantiates (via pooling) one enemy of type "type" on the borders of the level
    /// It also has the responsibility to inform the GameStateManager when a new asteroid is spawned
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    GameObject generateEnemy(enemyType type)
    {
        if (type == enemyType.Asteroid)
            GameStateManager.instance.AsteroidCreated();
        //get instance from pool
        GameObject result = enemyDataDict[type].pool.Allocate();
        //initialize instance
        result.transform.position = CoordinateUtility.getRandomBorderPosition(result.GetComponent<Collider2D>().bounds.extents.magnitude);
        //Debug.Log("Generated enemy: " + result.name + result.transform.position.ToString());
        return result;
    }
}
