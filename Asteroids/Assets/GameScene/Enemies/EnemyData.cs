[System.Serializable]
public class EnemyData
{
    public enemyType type;
    public Pool pool;
    public bool hasFrequency;
    public float totalSpawnTime;
}


public enum enemyType
{
    Asteroid,
    UfoBig,
    UfoSmall
}