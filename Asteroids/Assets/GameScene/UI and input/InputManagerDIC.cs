using System.Collections;
using UnityEngine;

public class InputManagerDIC : MonoBehaviour
{

    static directionInputReactionDelegate updateDirectionHolder;
    static buttonInputReactionDelegate activateFireHolder;
    static buttonInputReactionDelegate deactivateFireHolder;
    static buttonInputReactionDelegate activateThrustHolder;
    static buttonInputReactionDelegate deactivateThrustHolder;
    static buttonInputReactionDelegate activateHyperspaceHolder;
    static bool pendingRequest = false;

    public static void RegisterInputRequest(
        directionInputReactionDelegate updateDirection,
    buttonInputReactionDelegate activateFire,
    buttonInputReactionDelegate deactivateFire,
    buttonInputReactionDelegate activateThrust,
    buttonInputReactionDelegate deactivateThrust,
    buttonInputReactionDelegate activateHyperspace
        )
    {

        updateDirectionHolder = updateDirection;
        activateFireHolder = activateFire;
        deactivateFireHolder = deactivateFire;
        activateThrustHolder = activateThrust;
        deactivateThrustHolder = deactivateThrust;
        activateHyperspaceHolder = activateHyperspace;
        pendingRequest = true;

    }

    void Start()
    {
        if (pendingRequest)
            StartCoroutine(waitForInputManager());
    }

    IEnumerator waitForInputManager()
    {
        while (InputManager.instance == null)
            yield return new WaitForEndOfFrame();
        ExecuteInjection();
    }

    static void ExecuteInjection()
    {

        InputManager.updateDirection = updateDirectionHolder;
        InputManager.activateFire = activateFireHolder;
        InputManager.deactivateFire = deactivateFireHolder;
        InputManager.activateThrust = activateThrustHolder;
        InputManager.deactivateThrust = deactivateThrustHolder;
        InputManager.activateHyperspace = activateHyperspaceHolder;
        pendingRequest = false;
    }
}
