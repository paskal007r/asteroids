using UnityEngine;

public class JoystickManager : MonoBehaviour
{
    [SerializeField]
    RectTransform Handle;

    /// <summary>
    /// called from the Input manager, this method uses the touch position to place
    /// the Handle and then reads the direction the joystick has been pointed to,
    /// storing it in the JoystickDirection property for external usage
    /// </summary>
    /// <param name="touchPosition"></param>
    public void UpdateJoystickPosition(Vector2 touchPosition)
    {
        Handle.position = touchPosition;
        JoystickDirection = (Handle.anchoredPosition / ((RectTransform)Handle.transform.parent).rect.height) * 2;
        //Debug.Log(JoystickDirection.ToString());
    }

    public Vector2 JoystickDirection { get; private set; }

}
