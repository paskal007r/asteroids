using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void directionInputReactionDelegate(Vector2 direction);
public delegate void buttonInputReactionDelegate();

/*
 * This class was realized according to the dependency injection approach. The consumer 
 * class will attach its behaviours as delegates by passing them to the Dependency Injectior Class
 * InputManagerDIC and the Input Manager will deal with the UI and Touch interface in order
 * to call the appropriate reaction. 
 */
public class InputManager : MonoBehaviour
{
    #region variables
    #region Reaction delegates
    public static directionInputReactionDelegate updateDirection;
    public static buttonInputReactionDelegate activateFire;
    public static buttonInputReactionDelegate deactivateFire;
    public static buttonInputReactionDelegate activateThrust;
    public static buttonInputReactionDelegate deactivateThrust;
    public static buttonInputReactionDelegate activateHyperspace;
    #endregion

    public static InputManager instance;

    [SerializeField]
    Button FireButton;
    [SerializeField]
    Button ThrustButton;
    [SerializeField]
    Button HyperspaceButton;

    class buttonStatus
    {
        public bool lastFramePushed;
        public bool currentFramePushed;
    }
    Dictionary<Button, buttonStatus> buttonStatusDict = new Dictionary<Button, buttonStatus>();

    Vector2 JoystickDirection;
    [SerializeField]
    float deadZone;

    #endregion
    /// <summary>
    /// data initialization
    /// </summary>
    public void Awake()
    {

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        InitializeButtonDictData(FireButton);
        InitializeButtonDictData(ThrustButton);
        InitializeButtonDictData(HyperspaceButton);

        /* Test only
        updateDirection = delegate (Vector2 direction) { Debug.Log("direction:" + direction.ToString()); };
        activateFire = delegate () { Debug.Log("activateFire"); };
        deactivateFire = delegate () { Debug.Log("deactivateFire"); };
        activateThrust = delegate () { Debug.Log("activateThrust"); };
        deactivateThrust = delegate () { Debug.Log("deactivateThrust"); };
        activateHyperspace = delegate () { Debug.Log("activateHyperspace"); };
        */
    }


    void Update()
    {
        UpdateTouchData();
        if (JoystickDirection.magnitude > deadZone && updateDirection != null)
        {
            updateDirection(JoystickDirection);
            //Debug.Log(" updateDirection called ");
        }
        callButtonReaction(FireButton, activateFire, deactivateFire);
        callButtonReaction(ThrustButton, activateThrust, deactivateThrust);
        callButtonReaction(HyperspaceButton, activateHyperspace, null);
    }

    /// <summary>
    /// read all touch input, then update the JoystickDirection vector with the direction the joystick is
    /// pointing to and the buttonStatusDict dictionary with the status of each button
    /// </summary>
    void UpdateTouchData()
    {
        JoystickDirection = Vector2.zero;
        setLastFrameButtonStatus(FireButton);
        setLastFrameButtonStatus(ThrustButton);
        setLastFrameButtonStatus(HyperspaceButton);
        ClearButtonDictData(FireButton);
        ClearButtonDictData(ThrustButton);
        ClearButtonDictData(HyperspaceButton);

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                var touch = Input.GetTouch(i);
                HandleUIInteraction(touch.position);
            }
        }

#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            HandleUIInteraction(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        }
#endif
    }

    /// <summary>
    /// checks on the UI and updates the button status values or JoystickDirection
    /// </summary>
    /// <param name="position"></param>
    void HandleUIInteraction(Vector2 position)
    {
        GameObject touched = getTouchedUI(position);
        if (touched != null)
        {
            //Debug.Log("touched name " + touched.name);

            //if touched is a button 
            Button buttonComponent = touched.GetComponent<Button>();
            if (buttonComponent != null)
            {
                setCurrentFrameButtonStatus(buttonComponent);
            }
            else
            {//if touched is not a button, then it should be a joystick, otherwise it's a non-input UI element
                JoystickManager joystick;
                joystick = touched.GetComponentInParent<JoystickManager>();
                if (joystick == null)
                    joystick = touched.GetComponent<JoystickManager>();
                if (joystick != null)
                {
                    joystick.UpdateJoystickPosition(position);
                    JoystickDirection = joystick.JoystickDirection;
                }
            }
        }
    }

    /// <summary>
    /// returns the topmost UI object in the touchPosition coordinates
    /// </summary>
    /// <param name="touchPosition"></param>
    /// <returns></returns>
    public GameObject getTouchedUI(Vector2 touchPosition)
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);

        eventDataCurrentPosition.position = touchPosition;

        List<RaycastResult> tempRaycastResults = new List<RaycastResult>();

        EventSystem.current.RaycastAll(eventDataCurrentPosition, tempRaycastResults);
        if (tempRaycastResults.Count > 0)
        {
            Debug.Log("touching " + tempRaycastResults[0].gameObject.name);
            return tempRaycastResults[0].gameObject;
        }
        return null;
    }

    #region buttondictOperations
    void InitializeButtonDictData(Button button)
    {
        buttonStatusDict.Add(button, new buttonStatus() { lastFramePushed = false, currentFramePushed = false });
    }

    void ClearButtonDictData(Button button)
    {
        if (button != null)
        {
            if (buttonStatusDict.ContainsKey(button))
                buttonStatusDict[button].currentFramePushed = false;
            else
                Debug.LogError("missing button" + button.name);

        }
    }
    void setLastFrameButtonStatus(Button button)
    {

        if (buttonStatusDict.ContainsKey(button))
            buttonStatusDict[button].lastFramePushed = buttonStatusDict[button].currentFramePushed;
        else
            Debug.LogError("missing button " + button.name);
    }

    void setCurrentFrameButtonStatus(Button button)
    {
        if (buttonStatusDict.ContainsKey(button))
            buttonStatusDict[button].currentFramePushed = true;
        else
            Debug.LogError("unrecognized button!");
    }

    void callButtonReaction(Button toCheck, buttonInputReactionDelegate onActivation, buttonInputReactionDelegate onDeactivation)
    {
        if (buttonStatusDict.ContainsKey(toCheck))
        {
            if (buttonStatusDict[toCheck].lastFramePushed != buttonStatusDict[toCheck].currentFramePushed)
            {
                if (buttonStatusDict[toCheck].currentFramePushed && onActivation != null)
                    onActivation();

                if (!buttonStatusDict[toCheck].currentFramePushed && onDeactivation != null)
                    onDeactivation();
            }
        }

    }

    #endregion
}