using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ShowLifes : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    Text LifesText;

    public void Reset()
    {
        LifesText = GetComponent<Text>();
    }


    // Update is called once per frame
    void Update()
    {
        LifesText.text = GameStateManager.PlayerLifes.ToString();
    }
}
