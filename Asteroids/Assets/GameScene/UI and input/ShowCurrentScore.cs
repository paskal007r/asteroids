using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ShowCurrentScore : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    Text ScoreText;

    public void Reset()
    {
        ScoreText = GetComponent<Text>();
    }


    // Update is called once per frame
    void Update()
    {
        ScoreText.text = GameStateManager.Score.ToString();
    }
}
