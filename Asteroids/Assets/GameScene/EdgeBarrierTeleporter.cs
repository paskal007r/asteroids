using UnityEngine;

/*
 * This class implements a core behaviour of the game: when an object crosses the screen border is teleported on the opposite side of the screen
 */
public class EdgeBarrierTeleporter : MonoBehaviour
{
    [SerializeField]
    bool isVertical;



    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (isVertical)
            EdgeReaction(collision.transform, collision.bounds.extents.x);
        else
            EdgeReaction(collision.transform, collision.bounds.extents.y);
    }



    //Teleport the object on the opposite side of the screen
    public void EdgeReaction(Transform teleportable, float extension)
    {
        teleportable.position = CoordinateUtility.getOppositeBorderPosition(teleportable.position, isVertical, extension);
    }

}
