using UnityEngine;
/*
 * This class manages the Effect audio sources for the gameplay, in order to limit the amount of 
 * sounds that can be played at any given moment, both for performance and to avoid too much acoustic chaos.
 * Shot sounds have their dedicated and limited array of audio sources, so that they won't dominate the sound scene
 * blocking other requests.
 * Each sound that can be requested has its own Array of variations and is picked at random from there.
 * The sound samples were produced either with labchirp or bfxr, the music was made with sunvox for my ludum dare 35 entry.
 */
public class SoundManager : MonoBehaviour
{
    public AudioSource[] managedSources;
    public AudioSource[] shotDedicatedSources;
    public AudioClip[] shotVariations;
    public AudioClip[] asteroidDeathVariations;
    public AudioClip[] playerDeathVariations;
    public AudioClip[] ufoDeathVariations;
    public enum sounds
    {
        shot,
        asteroidDeath,
        playerDeath,
        ufoDeath
    }

    public static SoundManager instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    public void requestSound(sounds requested)
    {
        if (requested == sounds.shot)
        { AssignSource(shotDedicatedSources, requested); }
        else
            AssignSource(managedSources, requested);
    }

    void AssignSource(AudioSource[] soundChannel, sounds requested)
    {
        for (int i = 0; i < soundChannel.Length; i++)
        {
            if (!soundChannel[i].isPlaying)
            {
                soundChannel[i].clip = getClip(requested);
                soundChannel[i].Play();
            }
        }
    }

    AudioClip getClip(sounds toGet)
    {
        AudioClip result = null;

        switch (toGet)
        {
            case sounds.shot:
                result = shotVariations[Random.Range(0, shotVariations.Length)];
                break;
            case sounds.asteroidDeath:
                result = asteroidDeathVariations[Random.Range(0, asteroidDeathVariations.Length)];
                break;
            case sounds.playerDeath:
                result = playerDeathVariations[Random.Range(0, playerDeathVariations.Length)];
                break;
            case sounds.ufoDeath:
                result = ufoDeathVariations[Random.Range(0, ufoDeathVariations.Length)];
                break;
            default:
                Debug.LogError("uninplemented sound requested");
                break;
        }

        return result;
    }
}
