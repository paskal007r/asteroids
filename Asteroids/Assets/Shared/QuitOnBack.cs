#if UNITY_ANDROID
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitOnBack : MonoBehaviour
{
    public void Awake()
    {

        DontDestroyOnLoad(gameObject);
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            string sceneName = SceneManager.GetActiveScene().name;
            if (sceneName.Equals(LevelNames.GameScene.ToString()))
                LevelNames.MenuScene.Load();
            else if (sceneName.Equals(LevelNames.MenuScene.ToString()))
                Application.Quit();
            else
                Debug.LogError("Uninplemented scene reaction");
        }
    }
}
#endif
