using UnityEngine;

public class LoadSceneCaller : MonoBehaviour
{

    [SerializeField]
    LevelNames SceneToLoad;


    public void LoadScene()
    {
        SceneToLoad.Load();
    }
}
