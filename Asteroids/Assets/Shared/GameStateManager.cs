using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * This class has the responsibility to manage the state of the game at its most high level.
 * Holds the number of lifes left, the score and current level, it also handles the checks 
 * needed to update these variables as the game progresses.
 * Best score is saved using PlayerPrefs to ensure permanence.
 */

public class GameStateManager : MonoBehaviour
{
    public static int Score { get; private set; }
    int LastLifeGainScore;
    int ScoreToGainLife = 10000;
    public static int PlayerLifes { get; private set; }
    public static int BestScore { get; private set; }
    const string BestScoreKey = "BestScore";

    public static GameStateManager instance;

    public static int currentLevel { get; private set; }
    int currentAsteroids;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        BestScore = PlayerPrefs.GetInt(BestScoreKey, 0);
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// checks if the current level is the game scene, if so when the scene is loaded starts the game
    /// </summary>
    /// <param name="level"></param>
    public void OnLevelWasLoaded(int level)
    {
        //since I still have 5.3 there is no SceneManager.sceneLoaded yet, this method has been deprecated as of 5.4
        if (SceneManager.GetActiveScene().name.Equals(LevelNames.GameScene.ToString()))
            StartCoroutine(OnGameStart());
    }

    /// <summary>
    /// before actually starting the game wait for one frame so that all the rest of the initial setups is ensured to have been carried out
    /// Initializes the game state for a new game, then starts the level 0
    /// </summary>
    /// <returns></returns>
    IEnumerator OnGameStart()
    {
        yield return new WaitForEndOfFrame();
        Score = 0;
        PlayerLifes = 3;
        currentLevel = 0;
        currentAsteroids = 0;
        startNewLevel();
    }

    public void GainScore(int amount)
    {
        Score += amount;
        if (Score > LastLifeGainScore + ScoreToGainLife)
        {
            ++PlayerLifes;
            LastLifeGainScore = Score;
        }
    }

    public void OnPlayerDeath()
    {
        --PlayerLifes;
        if (PlayerLifes < 0)
        {
            OnGameOver();
        }
    }
    public void AsteroidCreated() { ++currentAsteroids; }
    public void AsteroidDestroyed()
    {
        --currentAsteroids;
        if (currentAsteroids <= 0)
        {
            ++currentLevel;
            startNewLevel();
        }

    }

    /// <summary>
    /// Calculates the number of enemies of each type in function of the current level 
    /// and calls on the EnemyFactory to populate the level
    /// </summary>
    public void startNewLevel()
    {

        Dictionary<enemyType, int> enemyAmounts = new Dictionary<enemyType, int>();

        //calculate enemies for this level
        enemyAmounts.Add(enemyType.Asteroid, 1 + Mathf.FloorToInt(currentLevel / 2f));
        enemyAmounts.Add(enemyType.UfoBig, Mathf.CeilToInt(currentLevel / 3f));
        enemyAmounts.Add(enemyType.UfoSmall, Mathf.CeilToInt(currentLevel / 5f));

        //use EnemyFactory to generate them
        EnemyFactory.instance.StartLevel(enemyAmounts);

    }

    /// <summary>
    /// when the game is over update the Best Score and record it, then load the Menu
    /// </summary>
    public void OnGameOver()
    {
        BestScore = Mathf.Max(BestScore, Score);
        PlayerPrefs.SetInt(BestScoreKey, BestScore);
        LevelNames.MenuScene.Load();
    }

}
