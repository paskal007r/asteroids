using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ShowBestScore : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    Text BestScoreText;

    public void Reset()
    {
        BestScoreText = GetComponent<Text>();
    }


    // Update is called once per frame
    void Update()
    {
        BestScoreText.text = GameStateManager.BestScore.ToString();
    }
}
