using UnityEngine.SceneManagement;

public enum LevelNames { MenuScene, GameScene }

public static class LevelLoad
{

    public static void Load(this LevelNames level)
    {
        SceneManager.LoadScene(level.ToString());
    }
}
