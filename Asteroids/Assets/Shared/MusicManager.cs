using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    // Use this for initialization
    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

}
